# 3088 UPS PiHAT

This is a Project to design a UPS for a course that I do.

The PiHat in this case is used as a UPS (Uninterruptible Power Supply). This allows for users to be able to save their work etc. during times of loadshedding for example.

Bill of materials:

-OPAMPS
-9.6V Battery

BJTS
LEDs
Resistors
Diodes

There are three subsystems namely:

Voltage Supply and Battery Charging Circuit
Current Shunt for monitoring the voltage
LED status indictors
